<?php
require_once '../vendor/autoload.php';
require_once '../src/Client.php';
new UI_DevOutput;

class App{

	public function __construct(){
		if( !file_exists( 'config.ini' ) )
			throw new RuntimeException( 'Config file "config.ini" is missing' );
		$this->config		= (object) parse_ini_file( 'config.ini' );
	}

	public static function create(){
		return new self();
	}

	public function run(){
		$this->client		= new Client( $this->config->apiToken );
		$this->request		= new Net_HTTP_Request_Receiver();
		$this->response		= new Net_HTTP_Response();
		$this->respond( $this->dispatch() );
	}

	protected function dispatch(){
		switch( strtolower( $this->request->get( 'action' ) ) ){
			case 'project':
				$project	= $this->request->get( 'project' );
				$data		= $this->client->getProjectDetails( $this->config->username, $project );
				$content	= '<h3>Project: '.$project.'</h3>'.print_m( $data, NULL, NULL, TRUE );
				break;
			case 'index':
			default:
				$data		= $this->client->listProjects();
				$rows		= array();
				foreach( $data as $item ){
					$rows[]	= UI_HTML_Tag::create( 'tr', array(
						UI_HTML_Tag::create( 'td', $item->user ),
						UI_HTML_Tag::create( 'td', UI_HTML_Tag::create( 'a', $item->name, array( 'href' => './?action=project&user='.$item->user.'&project='.$item->name ) ) ),
						UI_HTML_Tag::create( 'td', $item->commit->commit->grade ),
						UI_HTML_Tag::create( 'td', $item->commit->commit->nrIssues ),
					) );
				}
				$table		= UI_HTML_Tag::create( 'table', array(
					UI_HTML_Tag::create( 'thead', UI_HTML_Tag::create( 'tr', array(
						UI_HTML_Tag::create( 'th', 'User' ),
						UI_HTML_Tag::create( 'th', 'Project' ),
						UI_HTML_Tag::create( 'th', 'Grade' ),
						UI_HTML_Tag::create( 'th', 'Issues' ),
					) ) ),
					UI_HTML_Tag::create( 'tbody', $rows ),
				), array( 'class' => 'table' ) );
//				$content	= '<h3>Projects</h3>'.print_m( $data, NULL, NULL, TRUE );
				$content	= '<h3>Projects</h3>'.$table;
		}
		return $content;
	}

	protected function respond( $content ){
		$body	= '<div class="container"><h2>Codacy Client</h2><hr/>'.$content.'</div>';
		$page	= new UI_HTML_PageFrame();
		$page->addStylesheet( 'https://cdn.ceusmedia.de/css/bootstrap.min.css' );
		$page->addBody( $body );
		$this->response->setBody( $page->build() );
		Net_HTTP_Response_Sender::sendResponse( $this->response );
	}
}


try{
	App::create()->run();
} catch( Exception $e ){
	die( 'Error: '.$e->getMessage().'.'.PHP_EOL );
}

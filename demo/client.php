<?php
require_once '../vendor/autoload.php';
require_once '../src/Client.php';
new UI_DevOutput;

try{
	if( !file_exists( 'config.ini' ) )
		throw new RuntimeException( 'Config file "config.ini" is missing' );

	$config		= (object) parse_ini_file( 'config.ini' );
	$client		= new Client( $config->apiToken );

	remark( '1. listProjects:' );
	$response	= $client->listProjects();
	print_m( $response );

	remark( '2. getProjectDetails:' );
	$response	= $client->getProjectDetails( $config->username, $config->project );
	print_m( $response );

} catch( Exception $e ){
	die( 'Error: '.$e->getMessage().'.'.PHP_EOL );
}

<?php
class Client{

	protected $apiToken;
	protected $apiBaseUrl	= 'https://api.codacy.com/2.0/';

	public function __construct( $apiToken ){
		$this->apiToken	= $apiToken;
	}

	public function listProjects(){
		$this->checkSetup();
		$response	= $this->get( 'project/list' );
		return $response;
	}

	public function getProjectDetails( $username, $project ){
		return $this->get( $username.'/'.$project );
	}

	protected function get( $path ){
		$reader	= new Net_CURL( $this->apiBaseUrl.$path );
		$reader->setOption( CURLOPT_HTTPHEADER, array(
			'Accept: application/json',
			'api_token: '.$this->apiToken,
		) );
		$response	= $reader->exec();
		$data		= json_decode( $response );
		return $data;
	}

	protected function checkSetup(){
		if( !$this->apiToken )
			throw new RuntimeException( 'No API token set' );
	}
}
